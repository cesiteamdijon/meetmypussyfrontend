import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CardComponent} from './components/card/card.component';

import {
    MatButtonModule,
    MatCardModule
} from '@angular/material';

@NgModule({
    declarations: [CardComponent],
    imports: [
        CommonModule,
        MatCardModule,
        MatButtonModule
    ],
    exports: [CardComponent]
})
export class SharedModule {
}
