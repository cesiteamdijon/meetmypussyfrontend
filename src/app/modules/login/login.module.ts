import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './pages/login/login.component';
import {MatButtonModule, MatFormFieldModule, MatIconModule, MatInputModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LogoutComponent} from './pages/logout/logout.component';
import {RouterModule} from '@angular/router';

@NgModule({
    declarations: [LoginComponent, LogoutComponent],
    imports: [
        CommonModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        FormsModule,
        MatInputModule,
        MatIconModule,
        MatButtonModule,
        RouterModule
    ],
    exports: [LoginComponent]
})
export class LoginModule {
}
