import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {AuthService} from '../../../../core/services/auth/auth.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent {

    private hide: boolean;

    private connexionForm = new FormGroup({
        username: new FormControl(''),
        password: new FormControl(
            '',
            [
                Validators.minLength(4)
            ]),
    });

    constructor(private authService: AuthService, private router: Router) {

        this.hide = true;
    }

    get password() {
        return this.connexionForm.get('password');
    }

    onSubmit() {

        const result = this.authService.login(
            this.connexionForm.value.username,
            this.connexionForm.value.password
        ).subscribe(user => {

            if (this.authService.getConnexionState().getValue()) {
                this.router.navigate(['profile']);
            }
        });
    }
}
