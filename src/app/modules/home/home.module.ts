import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './pages/home/home.component';
import {SharedModule} from '../shared/shared.module';
import {MatButtonModule, MatCardModule} from '@angular/material';

@NgModule({
    declarations: [HomeComponent],
    imports: [
        CommonModule,
        SharedModule,
        MatCardModule,
        MatButtonModule
    ]
})
export class HomeModule {
}
