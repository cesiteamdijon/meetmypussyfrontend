import {Component, Input, OnInit} from '@angular/core';
import {Profile} from '../../../../core/models/profile';

@Component({
  selector: 'app-profile-infos',
  templateUrl: './profile-infos.component.html',
  styleUrls: ['./profile-infos.component.css']
})
export class ProfileInfosComponent implements OnInit {

  @Input()
  profile: Profile;

  constructor() { }

  ngOnInit() {
  }

}
