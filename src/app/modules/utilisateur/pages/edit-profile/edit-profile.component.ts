import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Profile} from '../../../../core/models/profile';
import {ProfileService} from '../../../../core/services/profile/profile.service';

@Component({
    selector: 'app-edit-profile',
    templateUrl: './edit-profile.component.html',
    styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {

    public hide: boolean;

    private connexionForm = new FormGroup({
        name: new FormControl(''),
        race: new FormControl(''),
        gender: new FormControl(''),
        coat: new FormControl(''),
        city: new FormControl(''),
        identification: new FormControl(''),
        description: new FormControl('')
    });

    constructor(private profileService: ProfileService, private router: Router) {
    }

    onSubmit() {

        const profile = new Profile(
            this.connexionForm.value.name,
            this.connexionForm.value.race,
            this.connexionForm.value.gender,
            this.connexionForm.value.coat,
            this.connexionForm.value.city,
            this.connexionForm.value.identification,
            this.connexionForm.value.description,
            null
        );

        const result = this.profileService
            .editProfile(profile)
            .subscribe(() => {
                this.router.navigate(['profile']);
            });
    }

    ngOnInit(): void {
        this.profileService.getCurrentProfile().subscribe(p => {
            this.connexionForm.controls.name.setValue(p.name);
            this.connexionForm.controls.race.setValue(p.race);
            this.connexionForm.controls.gender.setValue(p.gender);
            this.connexionForm.controls.coat.setValue(p.coat);
            this.connexionForm.controls.city.setValue(p.city);
            this.connexionForm.controls.identification.setValue(p.identification);
            this.connexionForm.controls.description.setValue(p.description);
        });
    }
}
