import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../../core/services/auth/auth.service';
import {Router} from '@angular/router';
import {RegisterUtilisateurOutputMessage} from '../../../../core/models/register-utilisateur-output-message';
import {Profile} from '../../../../core/models/profile';

@Component({
    selector: 'app-create-user',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent {

    private hide: boolean;

    private connexionForm = new FormGroup({
        name: new FormControl(''),
        race: new FormControl(''),
        gender: new FormControl(''),
        coat: new FormControl(''),
        city: new FormControl(''),
        identification: new FormControl(''),
        description: new FormControl(''),
        login: new FormControl(''),
        password: new FormControl(
            '',
            [
                Validators.minLength(4)
            ]),
    });

    constructor(private authService: AuthService, private router: Router) {

        this.hide = true;
    }

    get password() {
        return this.connexionForm.get('password');
    }

    onSubmit() {

        const user = new RegisterUtilisateurOutputMessage(
            null,
            this.connexionForm.value.login,
            this.connexionForm.value.password
        );

        const profile = new Profile(
            this.connexionForm.value.name,
            this.connexionForm.value.race,
            this.connexionForm.value.gender,
            this.connexionForm.value.coat,
            this.connexionForm.value.city,
            this.connexionForm.value.identification,
            this.connexionForm.value.description,
            null
        );

        const result = this.authService
            .register(user, profile)
            .subscribe(() => {

            if (this.authService.getConnexionState().getValue()) {
                this.router.navigate(['profile']);
            }
        });
    }

}
