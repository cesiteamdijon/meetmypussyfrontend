import {Component, OnInit} from '@angular/core';
import {ProfileService} from '../../../../core/services/profile/profile.service';
import {Profile} from '../../../../core/models/profile';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

    public profile: Profile = null;

    constructor(private profileService: ProfileService, private route: ActivatedRoute) {
    }

    ngOnInit() {

        this.route.params.subscribe(p => {

            if (!p.hasOwnProperty('id')) {

                this.profileService.getCurrentProfile().subscribe(pf => this.profile = pf);
            } else {

                this.profileService.getProfileById(p.id).subscribe(pf => this.profile = pf);
            }
        });
    }

}
