import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProfileComponent} from './pages/profile/profile.component';
import {RegisterComponent} from './pages/register/register.component';
import {MatButtonModule, MatFormFieldModule, MatIconModule, MatInputModule, MatTabsModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ProfileInfosComponent} from './components/profile-infos/profile-infos.component';
import {ProfilePhotosComponent} from './components/profile-photos/profile-photos.component';
import {RouterModule} from '@angular/router';
import { EditProfileComponent } from './pages/edit-profile/edit-profile.component';

@NgModule({
    declarations: [ProfileComponent, RegisterComponent, ProfileInfosComponent, ProfilePhotosComponent, EditProfileComponent],
    imports: [
        CommonModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        FormsModule,
        MatInputModule,
        MatIconModule,
        MatButtonModule,
        MatTabsModule,
        RouterModule
    ]
})
export class UtilisateurModule {
}
