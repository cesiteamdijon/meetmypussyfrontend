import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './modules/home/pages/home/home.component';
import {LoginComponent} from './modules/login/pages/login/login.component';
import {PimpGuard} from './core/guards/pimp.guard';
import {LogoutComponent} from './modules/login/pages/logout/logout.component';
import {ProfileComponent} from './modules/utilisateur/pages/profile/profile.component';
import {RegisterComponent} from './modules/utilisateur/pages/register/register.component';
import {EditProfileComponent} from './modules/utilisateur/pages/edit-profile/edit-profile.component';

const publicRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'logout', component: LogoutComponent },
    // { path: '**', component: PageNotFoundComponent }
];
const privateRoutes: Routes = [
    {
        path: 'index',
        component: HomeComponent,
        canActivate: [PimpGuard],
    },
    {
        path: 'profile',
        component: ProfileComponent,
        canActivate: [PimpGuard],
    },
    {
        path: 'profile/:id',
        component: ProfileComponent,
        canActivate: [PimpGuard],
    },
    {
        path: 'profile/edit',
        component: EditProfileComponent,
        canActivate: [PimpGuard],
    }
];


@NgModule({
    imports: [
        RouterModule.forRoot(publicRoutes),
        RouterModule.forRoot(privateRoutes)
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
