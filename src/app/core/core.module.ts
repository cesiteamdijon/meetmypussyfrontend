import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SidenavComponent} from './sidenav/sidenav.component';
import {
    MatAutocompleteModule,
    MatButtonModule, MatFormFieldModule,
    MatIconModule, MatInputModule,
    MatListModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule
} from '@angular/material';
import {AppRoutingModule} from '../app-routing.module';
import {BrowserModule} from '@angular/platform-browser';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AuthService} from './services/auth/auth.service';
import {PimpGuard} from './guards/pimp.guard';
import {ProfileService} from './services/profile/profile.service';
import {AuthInterceptor} from './interceptors/auth-interceptor';
import { GlobalSearchComponent } from './global-search/global-search.component';
import {ReactiveFormsModule} from '@angular/forms';
import { FormsModule } from '@angular/forms';
import {SearchService} from './services/search/search.service';


@NgModule({
    declarations: [SidenavComponent, GlobalSearchComponent],
    imports: [
        BrowserModule,
        CommonModule,
        MatButtonModule,
        MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatListModule,
        MatMenuModule,
        AppRoutingModule,
        HttpClientModule,
        MatAutocompleteModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule
    ],
    providers: [
        AuthService,
        PimpGuard,
        ProfileService,
        SearchService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true
        }
    ],
    exports: [SidenavComponent]
})
export class CoreModule {
}
