export class RegisterUtilisateurOutputMessage {
    constructor(
        public id: number,
        public email: string,
        public password: string
    ) { }
}
