import {Photo} from './photo';

export class Profile {

    constructor(
        public name: String,
        public race: String,
        public gender: String,
        public coat: String,
        public city: String,
        public identification: String,
        public description: String,
        public photos: Array<Photo>,
    ) { }
}
