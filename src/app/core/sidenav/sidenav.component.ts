import {Component, OnInit} from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AuthService} from '../services/auth/auth.service';
import {RegisterUtilisateurOutputMessage} from '../models/register-utilisateur-output-message';
import {ProfileService} from '../services/profile/profile.service';
import {Profile} from '../models/profile';

@Component({
    selector: 'app-sidenav',
    templateUrl: './sidenav.component.html',
    styleUrls: ['./sidenav.component.css'],
})
export class SidenavComponent implements OnInit {

    public isAuthenticated: boolean;

    public utilisateur: Profile = null;

    isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
        .pipe(
            map(result => result.matches)
        );

    constructor(
        private breakpointObserver: BreakpointObserver,
        private authService: AuthService,
        private profileService: ProfileService
    ) {
    }

    ngOnInit(): void {
        this.checkAuth(this.authService.getConnexionState().getValue());
        this.authService.getConnexionState().subscribe(this.checkAuth.bind(this));
    }

    checkAuth(isAuthenticated: boolean): void {

        this.isAuthenticated = isAuthenticated;

        if (isAuthenticated) {
            this.profileService
                .getCurrentProfile()
                .subscribe(profile => this.utilisateur = profile);
        }
    }
}
