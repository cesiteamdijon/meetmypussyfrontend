import {TestBed, async, inject} from '@angular/core/testing';

import {PimpGuard} from './pimp.guard';

describe('PimpGuard', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [PimpGuard]
        });
    });

    it('should ...', inject([PimpGuard], (guard: PimpGuard) => {
        expect(guard).toBeTruthy();
    }));
});
