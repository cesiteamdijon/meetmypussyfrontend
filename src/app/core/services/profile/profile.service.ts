import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Profile} from '../../models/profile';
import {environment} from '../../../../environments/environment';
import {map} from 'rxjs/operators';
import {AuthService} from '../auth/auth.service';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ProfileService {

    constructor(private http: HttpClient, private authService: AuthService) {
    }

    getCurrentProfile(): Observable<Profile> {

        return this.http
            .get<Profile>(
                `${environment.apiUrl}/profile`
            )
            .pipe(
                map(profile => {
                    return new Profile(
                        profile.name,
                        profile.race,
                        profile.gender,
                        profile.coat,
                        profile.city,
                        profile.identification,
                        profile.description,
                        profile.photos
                    );
                })
            );

    }

    getProfileById(idProfile: string): Observable<Profile> {

        return this.http
            .get<Profile>(
                `${environment.apiUrl}/profile/${idProfile}`
            )
            .pipe(
                map(profile => {
                    return new Profile(
                        profile.name,
                        profile.race,
                        profile.gender,
                        profile.coat,
                        profile.city,
                        profile.identification,
                        profile.description,
                        profile.photos
                    );
                })
            );

    }

    editProfile(profile: Profile) {

        return this.http.put<Profile>(
            `${environment.apiUrl}/profile`,
            {
                ...profile
            }
        );
    }
}
