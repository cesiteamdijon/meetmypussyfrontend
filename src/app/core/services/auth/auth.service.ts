import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthenticationToken} from '../../models/authentication-token';
import {RegisterUtilisateurOutputMessage} from '../../models/register-utilisateur-output-message';
import {environment} from '../../../../environments/environment';
import {Profile} from '../../models/profile';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    redirectUrl: string;

    private authToken: BehaviorSubject<AuthenticationToken>;

    private connexionState: BehaviorSubject<boolean>;

    constructor(private http: HttpClient) {

        const tokenStored = localStorage.getItem(`AuthToken-${environment.appKey}`);

        const token = tokenStored === null
            ? null
            : new AuthenticationToken(tokenStored);


        this.connexionState = new BehaviorSubject<boolean>(token !== null);
        this.authToken = new BehaviorSubject<AuthenticationToken>(token);

        this.authToken.subscribe(t => this.connexionState.next(t !== null));
        this.authToken.subscribe(AuthService.saveToken);
    }

    static saveToken(token: AuthenticationToken) {

        if (token === null) {
            localStorage.removeItem(`AuthToken-${environment.appKey}`);
        } else {
            localStorage.setItem(
                `AuthToken-${environment.appKey}`,
                token.token
            );
        }
    }

    login(username: string, password: string): Observable<AuthenticationToken> {

        return this.http
            .post<AuthenticationToken>(
                `${environment.apiUrl}/auth`,
                {login: username, password: password}
            )
            .pipe(
                map(message => {

                    if (!message.hasOwnProperty('token')) {
                        return null;
                    }

                    this.authToken.next(
                        new AuthenticationToken(message.token)
                    );
                })
            );
    }

    logout(): void {
        this.authToken.next(null);
    }

    getConnexionState(): BehaviorSubject<boolean> {
        return this.connexionState;
    }

    getToken(): AuthenticationToken {
        return this.authToken.getValue();
    }

    getAuthenticationToken(): BehaviorSubject<AuthenticationToken> {
        return this.authToken;
    }

    register(user: RegisterUtilisateurOutputMessage, profile: Profile): Observable<AuthenticationToken> {

        return this.http
            .post<AuthenticationToken>(
                `${environment.apiUrl}/register`,
                {
                    login: user.email,
                    password: user.password,
                    name: profile.name,
                    race: profile.race,
                    gender: profile.gender,
                    coat: profile.coat,
                    city: profile.city,
                    identification: profile.identification,
                    description: profile.description
                },
                {
                    headers: new HttpHeaders({
                        'Content-Type': 'application/json'
                    })
                }
            )
            .pipe(
                map(message => {

                    if (!message.hasOwnProperty('token')) {
                        return null;
                    }

                    this.authToken.next(
                        new AuthenticationToken(message.token)
                    );
                })
            );
    }
}
